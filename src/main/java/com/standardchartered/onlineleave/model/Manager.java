package com.standardchartered.onlineleave.model;

public class Manager extends Staff{

	public Manager(String firstName, String lastName, String staffNumber) {
		super(firstName, lastName, staffNumber);
	}
	
	
	public String toString() {
		return super.toString() + "\n\t\tDesignation\t: Manager";
	}

}
