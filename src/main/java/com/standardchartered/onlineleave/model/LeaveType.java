package com.standardchartered.onlineleave.model;

public class LeaveType {

	private int id;
	private String name;
	private String code;
	public static final int MAXIMUM_COUNT = 18;

	public LeaveType(int id, String code, String name) {
		this.id = id;
		this.name = name;
		this.code = code;
	}

	public LeaveType(String code, String name) {
		this.name = name;
		this.code = code;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String toString() {
		return "Leave Type : \n\t\tid\t: " + id + "\n\t\tLeave Code\t: " + code + "\n\t\tLeave Type\t: " + name;
	}

}
