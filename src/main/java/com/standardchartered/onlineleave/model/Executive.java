package com.standardchartered.onlineleave.model;

public class Executive extends Staff{

	public Executive(String firstName, String lastName, String staffNumber) {
		super(firstName, lastName, staffNumber);
	}
	
	public String toString() {
		return super.toString() + "\n\t\tDesignation\t: Executive";
	}
}
