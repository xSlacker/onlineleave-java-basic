package com.standardchartered.onlineleave.model;

public class LeaveSetup {

	private int id;
	private Staff staff;
	private LeaveType leaveType;
	private int eligible;
	private int applied;
	private int balance;
	private int year;

	public LeaveSetup(int id, Staff staff, LeaveType leaveType, int year, int eligible) {
		this.staff = staff;
		this.leaveType = leaveType;
		this.year = year;
		this.eligible = eligible;
		this.id = id;
	}

	public LeaveSetup(Staff staff, LeaveType leaveType, int year, int eligible) {
		this.staff = staff;
		this.leaveType = leaveType;
		this.year = year;
		this.eligible = eligible;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public int getEligible() {
		return eligible;
	}

	public void setEligible(int eligible) {
		this.eligible = eligible;
	}

	public int getApplied() {
		return applied;
	}

	public void setApplied(int applied) {
		this.applied = applied;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String toString() {
		return "Leave Setup :\n\t" + staff + "\n\t" + leaveType + "\n\tEligible\t: " + eligible + "\n\tYear\t\t: "
				+ year;
	}

}
