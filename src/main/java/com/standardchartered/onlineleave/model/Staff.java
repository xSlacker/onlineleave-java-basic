package com.standardchartered.onlineleave.model;

public class Staff {

	private int id;
	private String firstName;
	private String lastName;
	private String staffNumber;

	public Staff(int id, String firstName, String lastName, String staffNumber) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.staffNumber = staffNumber;
	}

	public Staff(String firstName, String lastName, String staffNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.staffNumber = staffNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStaffNumber() {
		return staffNumber;
	}

	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}

	public String toString() {
		return "Staff : \n\t\tid\t: " + id + "\n\t\tFirst Name\t: " + firstName + "\n\t\tLast Name\t: " + lastName
				+ "\n\t\tStaff Number\t: " + staffNumber;
	}

}
