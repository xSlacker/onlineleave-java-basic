package com.standardchartered.onlineleave.controller;

import java.util.Scanner;

import com.standardchartered.onlineleave.model.Staff;
import com.standardchartered.onlineleave.service.StaffService;

public class StaffController {

	Scanner scanner;

	public void getStaffs() {
		for (Staff staff : new StaffService().getStaffs()) {
			System.out.println(staff);
		}
	}

	public void viewStaff() {
		scanner = new Scanner(System.in);
		System.out.println("View Staff\nEnter the staff id : ");
		System.out.println(new StaffService().getStaffById(Integer.parseInt(scanner.nextLine().trim())));
		
	}

	public void deleteStaff() {
		scanner = new Scanner(System.in);
		System.out.println("Delete Staff\nEnter the staff id : ");
		new StaffService().deleteStaffById(Integer.parseInt(scanner.nextLine().trim()));
	}

	public int addStaff() {
		scanner = new Scanner(System.in);
		System.out.println("Enter the first name : ");
		String firstName = scanner.nextLine();
		System.out.println("Enter the last name : ");
		String lastName = scanner.nextLine();
		System.out.println("Enter the staff number : ");
		String staffNumber = scanner.nextLine();
		return new StaffService().addStaff(new Staff(firstName, lastName, staffNumber));
	}

	public void editStaff() {
		StaffService staffService = new StaffService();
		scanner = new Scanner(System.in);
		System.out.println("Edit Staff\nEnter the staff id : ");
		Staff staff = staffService.getStaffById(Integer.parseInt(scanner.nextLine().trim()));
		System.out.println("\n" + staff);
		System.out.println("\n-----------------------------------------\n");
		System.out.println("Enter the new first name : ");
		String input;
		if (!(input = scanner.nextLine()).isEmpty()) {
			staff.setFirstName(input);
		}
		System.out.println("Enter the new last name : ");
		if (!(input = scanner.nextLine()).isEmpty()) {
			staff.setLastName(input);
		}
		System.out.println("Enter the new staff number : ");
		if (!(input = scanner.nextLine()).isEmpty()) {
			staff.setStaffNumber(input);
		}
		System.out.println("\n" + staff);
		staffService.updateStaff(staff);
	}
}
