package com.standardchartered.onlineleave.controller;

import com.standardchartered.onlineleave.model.LeaveSetup;
import com.standardchartered.onlineleave.service.LeaveSetupService;

public class LeaveSetupController {
	public void getLeaveSetups() {
		for (LeaveSetup leaveSetup : new LeaveSetupService().getLeaveSetups()) {
			System.out.println(leaveSetup);
		}
	}

}
