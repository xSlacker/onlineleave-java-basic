package com.standardchartered.onlineleave.controller;

import java.util.Map.Entry;
import java.util.Scanner;

import com.standardchartered.onlineleave.model.LeaveType;
import com.standardchartered.onlineleave.service.LeaveTypeService;

public class LeaveTypeController {

	Scanner scanner;

	public void getLeaveTypes() {
		for (Entry<String, LeaveType> leaveTypeEntry : new LeaveTypeService().getLeaveTypes().entrySet()) {
			System.out.println(leaveTypeEntry.getValue());
		}

	}

	public void viewLeaveType() {
		scanner = new Scanner(System.in);
		System.out.println("View LeaveType\nEnter the leave type id : ");
		System.out.println(new LeaveTypeService().getLeaveTypeById(Integer.parseInt(scanner.nextLine().trim())));
	}

	public void deleteLeaveType() {
		scanner = new Scanner(System.in);
		System.out.println("Delete LeaveType\nEnter the leave type id : ");
		new LeaveTypeService().deleteLeaveTypeById(Integer.parseInt(scanner.nextLine().trim()));
	}

	public int addLeaveType() {
		scanner = new Scanner(System.in);
		System.out.println("Enter the leave type code : ");
		String code = scanner.nextLine();
		System.out.println("Enter the leave type name : ");
		String name = scanner.nextLine();
		return new LeaveTypeService().addLeaveType(new LeaveType(code, name));
	}

	public void editLeaveType() {
		LeaveTypeService leaveTypeService = new LeaveTypeService();
		scanner = new Scanner(System.in);
		System.out.println("Edit LeaveType\nEnter the leave type id : ");
		LeaveType leaveType = leaveTypeService.getLeaveTypeById(Integer.parseInt(scanner.nextLine().trim()));
		System.out.println("\n" + leaveType);
		System.out.println("\n-----------------------------------------\n");
		System.out.println("Enter the new leave type code : ");
		String input;
		if (!(input = scanner.nextLine()).isEmpty()) {
			leaveType.setCode(input);
		}
		System.out.println("Enter the new leave type name : ");
		if (!(input = scanner.nextLine()).isEmpty()) {
			leaveType.setName(input);
		}
		System.out.println("\n" + leaveType);
		leaveTypeService.updateLeaveType(leaveType);
	}
}
