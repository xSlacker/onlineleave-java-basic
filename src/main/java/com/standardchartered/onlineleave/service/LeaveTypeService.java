package com.standardchartered.onlineleave.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.standardchartered.onlineleave.model.LeaveType;
import com.standardchartered.onlineleave.utility.DbConnector;

public class LeaveTypeService {

	private final String QUERY_ALL = "SELECT * FROM leave_types";
	private final String QUERY_BY_ID = "SELECT * FROM leave_types WHERE id = ?";
	private final String QUERY_ADD = "INSERT INTO leave_types (code, name) VALUES(?, ?)";
	private final String QUERY_UPDATE = "UPDATE leave_types SET code = ? , name = ? WHERE id = ?";
	private final String QUERY_DELETE = "DELETE FROM leave_types WHERE id = ?";

	private PreparedStatement buildStatement(String query, Object... parameters) {
		try {
			PreparedStatement preparedStatement = DbConnector.connect().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			int paramIdx = 0;
			if (parameters != null) {
				for (Object object : parameters) {
					if (object instanceof Integer) {
						preparedStatement.setInt(++paramIdx, ((Integer) object));
					} else {
						preparedStatement.setString(++paramIdx, ((String) object));
					}
				}
			}
			return preparedStatement;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Map<String, LeaveType> getLeaveTypes() {
		Map<String, LeaveType> leaveTypeMap = new HashMap<String, LeaveType>();
		ResultSet resultset;
		try {
			resultset = buildStatement(QUERY_ALL).executeQuery();
			while (resultset.next()) {
				leaveTypeMap.put(resultset.getString("code"), new LeaveType(resultset.getInt("id"),
						resultset.getString("code"), resultset.getString("name")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return leaveTypeMap;
	}

	public LeaveType getLeaveTypeById(int id) {
		try {
			ResultSet resultset = buildStatement(QUERY_BY_ID, id).executeQuery();
			if (resultset.next()) {
				return new LeaveType(id, resultset.getString("code"), resultset.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int addLeaveType(LeaveType leaveType) {
		try {
			PreparedStatement preparedStatement = buildStatement(QUERY_ADD, leaveType.getCode(), leaveType.getName());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public void updateLeaveType(LeaveType leaveType) {
		try {
			buildStatement(QUERY_UPDATE, leaveType.getCode(), leaveType.getName(), leaveType.getId()).executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteLeaveTypeById(int id) {
		try {
			buildStatement(QUERY_DELETE, id).executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
