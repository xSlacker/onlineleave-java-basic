package com.standardchartered.onlineleave.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.standardchartered.onlineleave.model.Staff;
import com.standardchartered.onlineleave.utility.DbConnector;

public class StaffService {

	private final String QUERY_ALL = "SELECT * FROM staff";
	private final String QUERY_BY_ID = "SELECT * FROM staff WHERE id = ?";
	private final String QUERY_ADD = "INSERT INTO staff (firstname, lastname, staffnumber) VALUES(?, ?, ?)";
	private final String QUERY_UPDATE = "UPDATE staff SET firstname = ? , lastname = ?, staffnumber = ? WHERE id = ?";
	private final String QUERY_DELETE = "DELETE FROM staff WHERE id = ?";


	private PreparedStatement buildStatement(String query, Object... parameters) {
		try {
			PreparedStatement preparedStatement = DbConnector.connect().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			int paramIdx = 0;
			if (parameters != null) {
				for (Object object : parameters) {
					if (object instanceof Integer) {
						preparedStatement.setInt(++paramIdx, ((Integer) object));
					} else {
						preparedStatement.setString(++paramIdx, ((String) object));
					}
				}
			}
			return preparedStatement;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Staff> getStaffs() {
		List<Staff> staffList = new ArrayList<Staff>();
		try {
			ResultSet resultset = buildStatement(QUERY_ALL).executeQuery();
			while (resultset.next()) {
				staffList.add(new Staff(resultset.getInt("id"), resultset.getString("firstname"), resultset.getString("lastname"),
						resultset.getString("staffnumber")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return staffList;
	}
	
	
	public Staff getStaffById(int id) {
		try {
			ResultSet resultset = buildStatement(QUERY_BY_ID, id).executeQuery();
			if (resultset.next()) {
				return new Staff(resultset.getInt("id"),resultset.getString("firstname"), resultset.getString("lastname"),
						resultset.getString("staffnumber"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int addStaff(Staff staff) {
		try {
			PreparedStatement preparedStatement = buildStatement(QUERY_ADD, staff.getFirstName(), staff.getLastName(),
					staff.getStaffNumber());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public void updateStaff(Staff staff) {
		try {
			buildStatement(QUERY_UPDATE,  staff.getFirstName(), staff.getLastName(),
					staff.getStaffNumber(), staff.getId()).executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteStaffById(int id) {
		try {
			buildStatement(QUERY_DELETE, id).executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
