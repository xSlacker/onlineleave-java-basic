package com.standardchartered.onlineleave.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.standardchartered.onlineleave.model.LeaveSetup;
import com.standardchartered.onlineleave.model.LeaveType;
import com.standardchartered.onlineleave.model.Staff;
import com.standardchartered.onlineleave.utility.DbConnector;

public class LeaveSetupService {
	private final String QUERY_ALL = "SELECT * FROM leave_setups,leave_types,staff "
			+ "WHERE leave_setups.staff_id = staff.id AND leave_setups.leave_type_id = leave_types.id";

	private PreparedStatement buildStatement(String query, Object... parameters) {
		try {
			PreparedStatement preparedStatement = DbConnector.connect().prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			int paramIdx = 0;
			if (parameters != null) {
				for (Object object : parameters) {
					if (object instanceof Integer) {
						preparedStatement.setInt(++paramIdx, ((Integer) object));
					} else {
						preparedStatement.setString(++paramIdx, ((String) object));
					}
				}
			}
			return preparedStatement;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<LeaveSetup> getLeaveSetups() {
		List<LeaveSetup> leaveSetupList = new ArrayList<LeaveSetup>();
		ResultSet resultset;
		try {
			resultset = buildStatement(QUERY_ALL).executeQuery();
			while (resultset.next()) {
				leaveSetupList.add(new LeaveSetup(
						new Staff(resultset.getInt("staff_id"), resultset.getString("firstname"),
								resultset.getString("lastname"), resultset.getString("staffnumber")),
						new LeaveType(resultset.getInt("leave_type_id"), resultset.getString("code"),
								resultset.getString("name")),
						resultset.getInt("year"), resultset.getInt("eligible")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Returning " + leaveSetupList.size());
		return leaveSetupList;
	}

}
