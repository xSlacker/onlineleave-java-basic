package com.standardchartered.onlineleave.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {

	private List<MenuOption> menuOptions;
	private Scanner scanner;

	public Menu() {
		menuOptions = new ArrayList<MenuOption>();
	}

	public List<MenuOption> getMenuOptions() {
		return menuOptions;
	}

	public void setMenuOptions(List<MenuOption> menuOptions) {
		this.menuOptions = menuOptions;
	}

	public void addMenuOption(MenuOption menuOption) {
		menuOptions.add(menuOption);
	}

	public MenuOption handleEvent() {
		scanner = new Scanner(System.in);
		MenuOption selectedMenuOption = null;
		String choice = scanner.nextLine().trim();
		for (MenuOption menuOption : menuOptions) {
			if (choice.equals(String.valueOf(menuOption.getChoice()))) {
				selectedMenuOption = menuOption;
				break;
			}
		}
		return selectedMenuOption;
	}

	public String toString() {
		String res = "";
		for (MenuOption menuOption : menuOptions) {
			res += "[" + menuOption.getChoice() + "] - " + menuOption.getItem() + "\n";
		}
		return res + "\nPlease Select an Option : ";
	}
}
