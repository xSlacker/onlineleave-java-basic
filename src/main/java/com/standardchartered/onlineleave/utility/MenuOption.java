package com.standardchartered.onlineleave.utility;

public class MenuOption {

	private int choice;
	private String item;

	public MenuOption(int choice, String item) {
		this.choice = choice;
		this.item = item;
	}

	public int getChoice() {
		return choice;
	}

	public void setChoice(int choice) {
		this.choice = choice;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String toString() {
		return "Choice : " + item;
	}

	public void doEvent() {
	};

}
