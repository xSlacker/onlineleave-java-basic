package com.standardchartered.onlineleave.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {

	private static final String username = "root";
	private static final String password = "";
	private static final String database = "standardchartered";
	private static final String url = "jdbc:mysql://localhost:3306/" + database + "?serverTimezone=UTC";
	private static Connection connection;

	public static Connection connect() {
		try {
			if (connection == null)
				connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public static void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
