package com.standardchartered.onlineleave.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class DataUtility {

	public static List<String> getLines(String filename) {
		List<String> lines = new ArrayList<String>();
		BufferedReader bufferedReader = null;
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(filename);
			bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return lines;
	}

	public static void appendLine(String fileName, String aLine) {
		FileOutputStream fileOutputStream = null;
		BufferedWriter bufferedWriter = null;
		try {
			List<String> lines = getLines(fileName);
			fileOutputStream = new FileOutputStream(fileName);
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
			for (String line : lines) {
				bufferedWriter.write(line + "\n");
			}
			bufferedWriter.write(aLine);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedWriter != null) {
					bufferedWriter.close();
				}
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
