package com.standardchartered.onlineleave;

import com.standardchartered.onlineleave.controller.LeaveSetupController;
import com.standardchartered.onlineleave.controller.LeaveTypeController;
import com.standardchartered.onlineleave.controller.StaffController;
import com.standardchartered.onlineleave.utility.Menu;
import com.standardchartered.onlineleave.utility.MenuOption;

public class MainApplication {

	public static void main(String[] args) {
		showMenu();
	}

	private static void showMenu() {
		Menu menu = new Menu();
		menu.addMenuOption(new MenuOption(1, "Leave Type"));
		menu.addMenuOption(new MenuOption(2, "Staff"));
		menu.addMenuOption(new MenuOption(3, "Leave Setup"));
		menu.addMenuOption(new MenuOption(0, "Exit"));

		boolean isDone = false;
		menuloop: while (!isDone) {
			System.out.println("------------------------------------\n" + "Main Menu"
					+ "\n------------------------------------\n");
			System.out.println(menu);
			MenuOption selectedMenuOption = menu.handleEvent();
			if (selectedMenuOption == null) {
				System.out.println("Invalid option!");
				continue;
			}
			switch (selectedMenuOption.getChoice()) {
			case 1:
				isDone = doLeaveTypeMenu();
				break;
			case 2:
				isDone = doStaffMenu();
				break;
			case 3:
				isDone = doLeaveSetupMenu();
				break;
			case 0:
				break menuloop;
			}
		}

		System.out.println("Program ended.");

	}

	private static boolean doLeaveSetupMenu() {
		Menu menu = new Menu();
		menu.addMenuOption(new MenuOption(1, "List") {
			@Override
			public void doEvent() {
				new LeaveSetupController().getLeaveSetups();
			}
		});

		menu.addMenuOption(new MenuOption(9, "Back to Main Menu"));
		menu.addMenuOption(new MenuOption(0, "Exit"));

		while (true) {
			System.out.println("------------------------------------\n" + "Leave Setup Management"
					+ "\n------------------------------------\n");
			System.out.println(menu);
			MenuOption selectedMenuOption = menu.handleEvent();
			if (selectedMenuOption != null) {
				switch (selectedMenuOption.getChoice()) {
				case 9:
					return false;
				case 0:
					return true;
				default:
					selectedMenuOption.doEvent();
				}
			} else {
				System.out.println("Invalid Option!");
			}
		}
	}

	private static boolean doStaffMenu() {
		Menu menu = new Menu();
		menu.addMenuOption(new MenuOption(1, "List") {
			@Override
			public void doEvent() {
				new StaffController().getStaffs();
			}
		});
		menu.addMenuOption(new MenuOption(2, "View") {
			@Override
			public void doEvent() {
				new StaffController().viewStaff();
			}
		});
		menu.addMenuOption(new MenuOption(3, "Add") {
			@Override
			public void doEvent() {
				new StaffController().addStaff();
			}
		});
		menu.addMenuOption(new MenuOption(4, "Edit") {
			@Override
			public void doEvent() {
				new StaffController().editStaff();
			}
		});
		menu.addMenuOption(new MenuOption(5, "Delete") {
			@Override
			public void doEvent() {
				new StaffController().deleteStaff();
			}
		});
		menu.addMenuOption(new MenuOption(9, "Back to Main Menu"));
		menu.addMenuOption(new MenuOption(0, "Exit"));

		while (true) {
			System.out.println("------------------------------------\n" + "Staff Management"
					+ "\n------------------------------------\n");
			System.out.println(menu);
			MenuOption selectedMenuOption = menu.handleEvent();
			if (selectedMenuOption != null) {
				switch (selectedMenuOption.getChoice()) {
				case 9:
					return false;
				case 0:
					return true;
				default:
					selectedMenuOption.doEvent();
				}
			} else {
				System.out.println("Invalid Option!");
			}
		}
	}

	private static boolean doLeaveTypeMenu() {
		Menu menu = new Menu();
		menu.addMenuOption(new MenuOption(1, "List") {
			@Override
			public void doEvent() {
				new LeaveTypeController().getLeaveTypes();
			}
		});
		menu.addMenuOption(new MenuOption(2, "View") {
			@Override
			public void doEvent() {
				new LeaveTypeController().viewLeaveType();
			}
		});
		menu.addMenuOption(new MenuOption(3, "Add") {
			@Override
			public void doEvent() {
				new LeaveTypeController().addLeaveType();
			}
		});
		menu.addMenuOption(new MenuOption(4, "Edit") {
			@Override
			public void doEvent() {
				new LeaveTypeController().editLeaveType();
			}
		});
		menu.addMenuOption(new MenuOption(5, "Delete") {
			@Override
			public void doEvent() {
				new LeaveTypeController().deleteLeaveType();
			}
		});
		menu.addMenuOption(new MenuOption(9, "Back to Main Menu"));
		menu.addMenuOption(new MenuOption(0, "Exit"));

		while (true) {
			System.out.println("------------------------------------\n" + "Leave Type Management"
					+ "\n------------------------------------\n");
			System.out.println(menu);
			MenuOption selectedMenuOption = menu.handleEvent();
			if (selectedMenuOption != null) {
				switch (selectedMenuOption.getChoice()) {
				case 9:
					return false;
				case 0:
					return true;
				default:
					selectedMenuOption.doEvent();
				}
			} else {
				System.out.println("Invalid Option!");
			}
		}
	}
}
